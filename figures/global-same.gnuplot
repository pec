set term tikz color solid
set output "global-same.tex"
set nokey
set xrange [-10:10]
set yrange [-10:10]
set zrange [0:1]
set ztics 0,1,0.5
set isosample 20,20
splot 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
