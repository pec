set term tikz color solid
set output "local-disjoint.tex"
set nokey
set zrange [0:1]
set ztics 0,1,0.5
set xrange [-12.5:7.5]
set yrange [-7.5:0]
set isosample 30,30

f31(x,y) = (((5<x && x<7.5) && (-2.5<y && y<0)) ? 0 : 1/0)
f30(x,y) = (((5<x && x<7.5) && (-5<y && y<-2.5)) ? 0 : 1/0)
f3neg1(x,y) = (((5<x && x<7.5) && (-7.5<y && y<-5)) ? 0 : 1/0)
f21(x,y) = (((2.5<x && x<5) && (-2.5<y && y<0)) ? 0 : 1/0)
f20(x,y) = (((2.5<x && x<5) && (-5<y && y<-2.5)) ? 0 : 1/0)
f2neg1(x,y) = (((2.5<x && x<5) && (-7.5<y && y<-5)) ? 0 : 1/0)
f10(x,y) = (((0<x && x<2.5) && (-5<y && y<-2.5)) ? 0 : 1/0)
f00(x,y) = (((-2.5<x && x<0) && (-5<y && y<-2.5)) ? 0 : 1/0)
fneg10(x,y) = (((-5<x && x<-2.5) && (-5<y && y<-2.5)) ? 0 : 1/0)
fneg20(x,y) = (((-7.5<x && x<-5) && (-5<y && y<-2.5)) ? 0 : 1/0)
fneg31(x,y) = (((-10<x && x<-7.5) && (-2.5<y && y<0)) ? 0 : 1/0)
fneg30(x,y) = (((-10<x && x<-7.5) && (-5<y && y<-2.5)) ? 0 : 1/0)
fneg3neg1(x,y) = (((-10<x && x<-7.5) && (-7.5<y && y<-5)) ? 0 : 1/0)
fneg41(x,y) = (((-12.5<x && x<-10) && (-2.5<y && y<0)) ? 0 : 1/0)
fneg40(x,y) = (((-12.5<x && x<-10) && (-5<y && y<-2.5)) ? 0 : 1/0)
fneg4neg1(x,y) = (((-12.5<x && x<-10) && (-7.5<y && y<-5)) ? 0 : 1/0)
splot f31(x,y) notitle, \
      f30(x,y) notitle, \
      f3neg1(x,y) notitle, \
      f21(x,y) notitle, \
      f20(x,y) notitle, \
      f2neg1(x,y) notitle, \
      f10(x,y) notitle, \
      f00(x,y) notitle, \
      fneg41(x,y) notitle, \
      fneg40(x,y) notitle, \
      fneg4neg1(x,y) notitle, \
      fneg31(x,y) notitle, \
      fneg30(x,y) notitle, \
      fneg3neg1(x,y) notitle, \
      fneg20(x,y) notitle, \
      fneg10(x,y) notitle
