set term tikz color solid
set output "local-overlap.tex"
set nokey
set zrange [0:1]
set ztics 0,1,0.5
set xrange [-13.75:8.75]
set yrange [-8.75:1.25]
set isosample 30,30

f31(x,y) = (((3.75<x && x<8.75) && (-3.75<y && y<1.25)) ? 0 : 1/0)
f30(x,y) = (((3.75<x && x<8.75) && (-6.25<y && y<-1.25)) ? 0 : 1/0)
f3neg1(x,y) = (((3.75<x && x<8.75) && (-8.75<y && y<-3.75)) ? 0 : 1/0)
f21(x,y) = (((1.25<x && x<6.25) && (-3.75<y && y<1.25)) ? 0 : 1/0)
f20(x,y) = (((1.25<x && x<6.25) && (-6.25<y && y<-1.25)) ? 0 : 1/0)
f2neg1(x,y) = (((1.25<x && x<6.25) && (-8.75<y && y<-3.75)) ? 0 : 1/0)
f10(x,y) = (((-1.25<x && x<3.75) && (-6.25<y && y<-1.25)) ? 0 : 1/0)
f00(x,y) = (((-3.75<x && x<1.25) && (-6.25<y && y<-1.25)) ? 0 : 1/0)
fneg41(x,y) = (((-13.75<x && x<-8.75) && (-3.75<y && y<1.25)) ? 0 : 1/0)
fneg40(x,y) = (((-13.75<x && x<-8.75) && (-6.25<y && y<-1.25)) ? 0 : 1/0)
fneg4neg1(x,y) = (((-13.75<x && x<-8.75) && (-8.75<y && y<-3.75)) ? 0 : 1/0)
fneg31(x,y) = (((-11.25<x && x<-6.25) && (-3.75<y && y<1.25)) ? 0 : 1/0)
fneg30(x,y) = (((-11.25<x && x<-6.25) && (-6.25<y && y<-1.25)) ? 0 : 1/0)
fneg3neg1(x,y) = (((-11.25<x && x<-6.25) && (-8.75<y && y<-3.75)) ? 0 : 1/0)
fneg20(x,y) = (((-8.75<x && x<-3.75) && (-6.25<y && y<-1.25)) ? 0 : 1/0)
fneg10(x,y) = (((-6.25<x && x<-1.25) && (-6.25<y && y<-1.25)) ? 0 : 1/0)
splot f31(x,y) notitle, \
      f30(x,y) notitle, \
      f3neg1(x,y) notitle, \
      f21(x,y) notitle, \
      f20(x,y) notitle, \
      f2neg1(x,y) notitle, \
      f10(x,y) notitle, \
      f00(x,y) notitle, \
      fneg41(x,y) notitle, \
      fneg40(x,y) notitle, \
      fneg4neg1(x,y) notitle, \
      fneg31(x,y) notitle, \
      fneg30(x,y) notitle, \
      fneg3neg1(x,y) notitle, \
      fneg20(x,y) notitle, \
      fneg10(x,y) notitle
