BASE:=physical-evolutionary-computation
ROOT:=$(BASE).tex
RESULT:=$(BASE).pdf

ALLDEP:=Makefile

all:	$(RESULT)

$(RESULT):	$(ROOT) $(ALLDEP)
	pdflatex --interaction=nonstopmode $(ROOT)
	bibtex $(BASE)

clean:	FORCE
	rm -f *~ *.aux *.ent *.log *.blg *.toc


realclean:	clean
	rm -f $(PDF) *.bbl

.PHONY:	FORCE

